<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPackagesPackagist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        if (!Schema::hasTable('packagists')) {
            Schema::create('packagists', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->string('url');
                $table->boolean('satis')->default(0);
            });
        }

        if (!Schema::hasTable('packagists')) {

            Schema::create('packagists', function (Blueprint $table) {
                $table->id();
                $table->foreignId('packagist_id')->constrained('packagists')->onDelete('cascade');
                $table->foreignId('package_id')->constrained('packages')->onDelete('cascade');
                $table->string('version');
                $table->timestamps();
                $table->string('url');
            });
        }


        Schema::table('packages', function (Blueprint $table) {
            $table->foreignId('packagist_id')->nullable()->constrained('packagists')->onDelete('set null');
            $table->text('meta')->nullable();
            $table->string('repository')->nullable()->change();
        });


        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('packagist_versions');
        Schema::dropIfExists('packagists');
        Schema::table('packages', function (Blueprint $table) {
            $table->dropForeign('packages_packagist_id_foreign');
            $table->dropColumn('packagist_id');
            $table->dropColumn('meta');
        });

        Schema::enableForeignKeyConstraints();
    }
}
