<?php

namespace KDA\PackageManager;

use KDA\Laravel\PackageServiceProvider;
use GrahamCampbell\GitLab\Facades\GitLab;
class ServiceProvider extends PackageServiceProvider
{

    use \KDA\Laravel\Traits\HasViews;

    use \KDA\Laravel\Traits\HasLoadableMigration;
    use \KDA\Laravel\Traits\HasRoutes;
    use \KDA\Laravel\Traits\HasDumps;
    protected $viewNamespace = 'kda-packagemanager';
    protected $publishViewsTo = 'vendor/backpack';
    protected $dumps=[
        'package_logs',
        'packages',
    
    ];
    protected $routes = [
        'backpack/packagemanager.php'
    ];


    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    public function register(){
        parent::register();
        $this->app->singleton('kda.gitlab.integrationapi',function($app){
            return new \KDA\PackageManager\IntegrationAPI(Gitlab::connection());
        });
    }
}
