<?php 
namespace KDA\PackageManager\Facades;

use Illuminate\Support\Facades\Facade;


class IntegrationAPI extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'kda.gitlab.integrationapi'; }
}