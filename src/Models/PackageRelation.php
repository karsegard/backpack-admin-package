<?php

namespace KDA\PackageManager\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;


class PackageRelation extends Model 
{

    use CrudTrait;
   
    use \KDA\Taggable\Models\Traits\HasTag;

    protected $fillable = [
        'name',

    ];
    protected $casts = [
        'id' => 'integer',
    ];

    
   
    public function packages()
    {
        return $this->belongsToMany(Package::class,'package_relateds','relation_id','package_id','id','id');
    }
    
    public function relatedPackages()
    {
        return $this->belongsToMany(Package::class,'package_relateds','relation_id','related_package_id','id','id');
    }
}
