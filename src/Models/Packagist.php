<?php

namespace KDA\PackageManager\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;


class Packagist extends Model 
{

    use CrudTrait;
   

    protected $fillable = [
        'url',
        'satis',
        'created_at',
        'updated_at'

    ];
    protected $casts = [
        
        'satis'=>'boolean'
    ];

     
    public function packages()
    {
        return $this->hasMany(Package::class);
    }

}
