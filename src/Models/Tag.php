<?php
namespace KDA\PackageManager\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;


class Tag extends \KDA\Taggable\Models\Tag
{

    use CrudTrait;
  
}
