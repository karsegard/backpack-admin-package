<?php

namespace KDA\PackageManager\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;


class PackagistVersion extends Model 
{

    use CrudTrait;
   

    protected $fillable = [
        
    ];
    protected $casts = [
        
    ];

     
    public function packages()
    {
        return $this->belongsTo(Package::class);
    }


}
