<?php

namespace KDA\PackageManager\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;


class Package extends Model 
{

    use CrudTrait;
   
    use \KDA\Taggable\Models\Traits\HasTag;

    protected $fillable = [
        'slug',
        'repository',
        'scopes',
        'deprecated',
        'meta',
        'packagist_id'

    ];
    protected $casts = [
        'id' => 'integer',
        'scopes' => 'array',
        'deprecated'=>'boolean',
        'meta'=>'array'
    ];

     
    public function relatedPackages()
    {
        return $this->belongsToMany(Package::class,'package_relateds','package_id','related_package_id','id','id')->withPivot('relation_id');
    }

    public function referringPackages()
    {
        return $this->belongsToMany(Package::class,'package_relateds','related_package_id','package_id','id','id')->withPivot('relation_id');
    }

    public function identifiableAttribute()
    {
        return 'slug';
    }
    public function packagist(){
        return $this->belongsTo(Packagist::class);
    }
}
