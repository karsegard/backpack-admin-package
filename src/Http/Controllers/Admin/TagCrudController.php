<?php

namespace KDA\PackageManager\Http\Controllers\Admin;

use  KDA\PackageManager\Http\Requests\PackageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use GrahamCampbell\GitLab\Facades\GitLab;
use \KDA\PackageManager\Facades\IntegrationAPI;

/**
 * Class TransportersCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TagCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation ;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;



    public function setup()
    {
        $this->crud->setModel('\KDA\PackageManager\Models\Tag');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/kdapackagetag');
        $this->crud->setEntityNameStrings('tag', 'tags');
    }

    protected function setupListOperation()
    {
        
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PackageRequest::class);
        CRUD::field('name');
      
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }



}
