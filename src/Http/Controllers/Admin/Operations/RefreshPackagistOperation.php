<?php

namespace KDA\PackageManager\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait RefreshPackagistOperation
{
 
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupRefreshPackagistRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/refreshpackagist', [
            'as'        => $routeName . '.refreshpackagist',
            'uses'      => $controller . '@refreshpackagist',
            'operation' => 'refreshpackagist',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupRefreshPackagistDefaults()
    {
        $this->crud->allowAccess('refreshpackagist');
        $this->crud->setOperationSetting('label', 'Retour', 'sc_refreshpackagist');
        $this->crud->setOperationSetting('route', '', 'sc_refreshpackagist');
        $this->crud->operation('refreshpackagist', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('top', 'refreshpackagist', 'view', 'kda-packagemanager::backpack.crud.buttons.refreshpackagist');
        });
    }



    /**
     * Show the view for performing the operation.
     *
     * @return Response
     */
    public function refreshpackagist()
    {
      
        return back();
      
    }
}
