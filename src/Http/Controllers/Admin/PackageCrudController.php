<?php

namespace KDA\PackageManager\Http\Controllers\Admin;

use  KDA\PackageManager\Http\Requests\PackageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\CrudPanel\Traits\Operations;

use KDA\PackageManager\Jobs\CreateGitLabRepository;
/**
 * Class TransportersCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PackageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    } //IMPORTANT HERE
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    } //IMPORTANT HERE
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;


    use \KDA\PackageManager\Http\Controllers\Admin\Operations\RefreshPackagistOperation;

    public function fetchTags(){
        return $this->fetch(\KDA\PackageManager\Models\Tag::class);
    }

    public function setup()
    {
        $this->crud->setModel('KDA\PackageManager\Models\Package');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/kdapackagemanager');
        $this->crud->setEntityNameStrings('package', 'packages');
    }

    protected function setupListOperation()
    {
        $this->crud->enableExportButtons();

        CRUD::column('slug')->limit(255);
        CRUD::column('deprecated');
        CRUD::addColumn([
            'type'=>'closure',
            'label'=>'Has repository',
            'function'=> function($entry){
                return $entry->packagist ? ' oui' : 'non';
            },
            
        ]);
        
        CRUD::column('tags')->attribute('name');
    }


    protected function setupCommonFields(){
        CRUD::field('slug')->label('slug');
        CRUD::field('repository')->label('repository');;
        CRUD::addField([
            'name'=>'tags',
            'type'=>'relationship',
            'label'=> 'Tags',

            'ajax'          => true,
            'inline_create' => [ 'entity' => 'kdapackagetag' ]
        ]);
        CRUD::addField([
            'name' => 'deprecated',
            'label'=> 'Deprecated',

            'type' => 'toggle-label',
            'view_namespace' => 'kda-backpack-custom-fields::fields',
        ]);

        CRUD::addField([
            'name'=>'packagist',
            'type'=>'relationship',
            'label'=> 'Package repository',
        ]);
        
        CRUD::addField([
            'name' => 'relatedPackages',
            'type' => 'relationship',
            'label'=> 'Related',
            
            'subfields'   => [
               
                [
                    'name' => 'relation_id',
                    'type'      => 'select2',
                    'model'     => "KDA\PackageManager\Models\PackageRelation", // foreign key model
                    'attribute'=>'name',
                    'wrapper' => [
                        'class' => 'form-group col-md-9',
                    ],
                ],
            ],
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PackageRequest::class);
        $this->setupCommonFields();
        CRUD::addField([
            'name' => 'create_repository',
            'type' => 'toggle-label',
            'label'=> 'Create Repository',
            'tab'=> 'Repository',
            'view_namespace' => 'kda-backpack-custom-fields::fields',
        ]);
        CRUD::addField([
            'name' => 'public',
            'type' => 'toggle-label',
            'label'=> 'Public',

            'tab'=> 'Repository',
            'view_namespace' => 'kda-backpack-custom-fields::fields',
        ]);
        CRUD::addField(
            [
                'name' => 'import_url',
                'label'=> 'Import URL',
    
                'type' => 'select_from_array',
                'tab'=> 'Repository',
                'options' => [
                    'https://gitlab.com/karsegard/kda-skel-laravel-package.git' => 'karsegard/kda-skel-laravel-package',
                    'https://gitlab.com/karsegard/kda-skel-backpack-admin.git' => 'karsegard/kda-skel-laravel-backpack-package',
                    'https://gitlab.com/karsegard/react-webcomponent-skel.git' => 'karsegard/react-webcomponent',
                    'https://github.com/spatie/package-skeleton-laravel' => 'spatie/package',
                ],
                'default' => NULL
            ]
        );

        CRUD::addField([
            'type'=>"hidden",
            'name'=>'meta'
        ]);
        
        CRUD::addField([
            'name' => 'gitlab_group',
            'label'=> 'Gitlab Group',

            'type' => 'select_from_array',
            'tab'=> 'Repository',
            'options' => [
                '11714224' => 'karsegard',
                '11188514' => 'ecovisuel',
                '11188578' => 'geekagency',

            ],
            'default' => '11714224'
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCommonFields();

    }



    public function store()
    {
        $create = request('create_repository');
        $group = request('gitlab_group');
        $public = request('public');
        $import_url = request('import_url');
        $initialize_with_readme = $import_url ? false: true;

        $meta = [
            "create" => $create,
            "gitlab_group"=>$group,
            'public'=>$public,
            
            'import_url'=>$import_url,
            'initialize_with_readme'=> $initialize_with_readme,
            'created'=>false

        ];
       
        if ($create) {

            $this->crud->getRequest()->request->set('meta',$meta);
        }

        $response = $this->traitStore();

        if($create){
            //@TODO should be moved to Observer
            CreateGitLabRepository::dispatch($this->crud->entry );
        }


        return $response;
    }

    public function update()
    {

     
        $response = $this->traitUpdate();


        return $response;
    }
}
