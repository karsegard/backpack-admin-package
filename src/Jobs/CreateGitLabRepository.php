<?php

namespace KDA\PackageManager\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use GrahamCampbell\GitLab\Facades\GitLab;
use \KDA\PackageManager\Facades\IntegrationAPI;

class CreateGitLabRepository implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $package;
    public function __construct(\KDA\PackageManager\Models\Package $package)
    {
        $this->package = $package;
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $meta = $this->package->meta;
        if ($meta) {
            $create = $meta['create'];
            $created = $meta['created'];
            if ($create && !$created) {
                $group = $meta['gitlab_group'];
                $public = $meta['public'];
                $import_url = $meta['import_url'];

                $initialize_with_readme = $import_url ? false : true;

                $options = [];

                $project_name = $this->package->slug;
                $pos = strpos($project_name, '/');
                if ($pos !== false) {
                    $project_name = substr($project_name, $pos + 1);
                }

                if (!empty($import_url)) {
                    $options['import_url'] = $import_url;
                }
                
                $project = GitLab::projects()->create(
                    $project_name,
                    array_merge([
                        'namespace_id' => $group,
                        'default_branch' => 'master',
                        'visibility' => $public ? 'public' : 'private',
                        'initialize_with_readme' => $initialize_with_readme

                    ], $options)
                );

                if ($public) {
                    //$test = new \KDA\PackageManager\IntegrationAPI(Gitlab::connection());
                    $response = IntegrationAPI::addPackagist($project['id'], [
                        'username' => 'fdt2k',
                        'token' => '7IeQkwRODVHOL41nPkgV',
                        'tag_push_events' => true
                    ]);
                }
                $this->package->repository = $project['web_url'];
                $meta['created'] = true;
                $this->package->meta = $meta;
                $this->package->save();
            }
        }
    }
}
