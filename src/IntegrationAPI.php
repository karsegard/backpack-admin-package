<?php


namespace KDA\PackageManager;


class IntegrationAPI extends \Gitlab\Api\AbstractApi{

    /**
     * @param string $name
     * @param array  $parameters
     *
     * @return mixed
     */
    public function addPackagist($id, array $parameters = [])
    {
       

        return $this->put('/projects/'.$id.'/integrations/packagist', $parameters);
    }
}