<?php



Route::group([
    'namespace'  => 'KDA\PackageManager\Http\Controllers\Admin',
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', backpack_middleware()],
], function () {
    Route::crud('kdapackagemanager', 'PackageCrudController');
    Route::crud('kdapackagetag', 'TagCrudController');

});
